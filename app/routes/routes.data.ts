import { Route } from "./routes.types";
import movieRouter from '../modules/movies/movies.routes';
export const routes: Route[]=[
     new Route("/movie", movieRouter)
 ]