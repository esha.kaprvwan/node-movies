import { Router } from "express"

// export class Route {
//     path: string;
//     router: Router;

//     constructor(path: string, router: Router) {
//         this.path = path;
//         this.router = router;
//     }
// }
export class Route{
    constructor(
        public path:string,
        public router: Router
    ){}
}