import { IMovie } from "./movies.types";

class MovieSchema {
    Allmovies: IMovie[] = [];

    create(movie: IMovie) {
        this.Allmovies.push(movie);
        return " movie added "
    }
    show(){
        return this.Allmovies;
    }
    showOne(movieId : IMovie["Id"]){
        let index=this.Allmovies.findIndex(movies => movies.Id==movieId);
        if(index>-1)
        {
            return this.Allmovies[index]
        }
        else 
        {
            return " movie not found"
        }
    }
    deleteMovie(movieId : IMovie["Id"]){
        let index=this.Allmovies.findIndex(movies => movies.Id==movieId);
        if(index>-1)
        {
            this.Allmovies.splice(index,1)
        }
        else 
        {
            return " movie not found"
        }

    }
    updateMovie(Updatemovie: IMovie){
        let index=this.Allmovies.findIndex(movies => Updatemovie.Id==movies.Id);
        if(index>-1)
        {
            this.Allmovies[index]=Updatemovie;
        }
        else 
        {
            return 'Movie not found'
        }


    }
}

const Movie = new MovieSchema();

export default Movie;