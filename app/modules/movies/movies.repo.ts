import { IMovie } from "./movies.types";
import MovieSchemaDB from './movies.schema';

const createMovie = (movie: IMovie) => MovieSchemaDB.create(movie);
const showMovie=()=> MovieSchemaDB.show();
const showOne=(movieId : IMovie["Id"]) => MovieSchemaDB.showOne(movieId);
const updateMovie= (movieId:IMovie["Id"]) => MovieSchemaDB.updateMovie(movieId);
const deleteMovie=(movieId : IMovie["Id"]) => MovieSchemaDB.deleteMovie(movieId)
export default {
    createMovie,
    showMovie,
    showOne,
    updateMovie,
    deleteMovie
}