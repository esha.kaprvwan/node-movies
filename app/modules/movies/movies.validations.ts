import { body } from "express-validator";

// name: string;
// Rating: number;

// // Genere: string;
export const CreateMovieValidator=[
    body('name').isString().withMessage('name is required '),
    body('Rating').isInt().withMessage('Rating is required '),
    body('Genre').isString().withMessage('Genre is required ')
];