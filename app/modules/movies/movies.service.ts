import movieRepo from "./movies.repo"
import { IMovie } from "./movies.types"

const createMovie=(movie: IMovie) => movieRepo.createMovie(movie)
const showMovie=()=> movieRepo.showMovie()
const showOne= (movieId:IMovie["Id"])=> movieRepo.showOne(movieId)
const updateMovie =(movieId:IMovie["Id"]) => movieRepo.updateMovie(movieId)
const deleteMovie=(movieId:IMovie["Id"]) => movieRepo.deleteMovie(movieId)
export default{
    createMovie,
    showMovie,
    showOne,
    updateMovie,
    deleteMovie
}