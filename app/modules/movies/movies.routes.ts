// routes->service->repo->schema
import { Router, Request, Response, NextFunction } from "express";
import { ResponseHandler } from "../../utility/response.types"
import { CreateMovieValidator} from "./movies.validations"
import movieService from "./movies.service"
import { IMovie } from "./movies.types";
const router=Router();
router.post("/", CreateMovieValidator,(
    req: Request,
    res: Response,
    next: NextFunction
)=>{
    try{
        const movie=req.body;
        const result=movieService.createMovie(movie);
        res.send(new ResponseHandler(result));
        // res.send("yes")

    }catch(e){
        res.status(500).send(new ResponseHandler(null,e));

    }

})
router.get('/show', (
req: Request,
res: Response,
next: NextFunction
)=>{
try{
    
    const result=movieService.showMovie();
    res.send(new ResponseHandler(result));

}catch(e){
    res.status(500).send(new ResponseHandler(null,e));

}

})

router.get('/', (
    req: Request,
    res: Response,
    next: NextFunction
    )=>{
    try{
        const movieId =req.query.Id;
        const result=movieService.showOne(movieId);
        res.send(new ResponseHandler(result));
    
    }catch(e){
        res.status(500).send(new ResponseHandler(null,e));
    
    }
    
    })
    
    router.put('/', CreateMovieValidator,(
        req: Request,
        res: Response,
        next: NextFunction
        )=>{
        try{
            const movie=req.body;
            const result=movieService.updateMovie(movie);
            res.send(new ResponseHandler(result));
        
        }catch(e){
            res.status(500).send(new ResponseHandler(null,e));
        
        }
        
        })

        router.delete('/',(
            req: Request,
            res: Response,
            next: NextFunction
            )=>{
            try{
                const movieId =req.query.Id;
                const result=movieService.deleteMovie(movieId);
                res.send(new ResponseHandler(result));
            
            }catch(e){
                res.status(500).send(new ResponseHandler(null,e));
            
            }
            
            })
export default router;